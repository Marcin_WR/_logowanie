<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formularz Logowania</title>
    <link rel="stylesheet" href="public/style.css">
</head>

<body>
    <div class="page">
        <?php require_once("templates/pages/$page.php"); ?>
    </div>

</body>

</html