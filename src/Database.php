<?php

declare(strict_types=1);

namespace App;

use PDOException;
use Throwable;
use PDO;

class Database
{
    private PDO $connection;
    public function __construct(array $config)
    {
        try {
            $this->validateConfg($config);
            $this->createConnection($config);
        } catch (Throwable $e) {
            echo "blad";
        }
    }
    public function loginPassword($login = [], $haslo = []): array
    {
        $query = "SELECT*FROM uzytkownicy WHERE user='$login' AND pass='$haslo'";
        $result = $this->connection->query($query);
        $note = $result->fetch((PDO::FETCH_ASSOC));
        if ($note != false) {
            $fff = $note;
        } else {
            $fff = [];
        }
        return $fff;
    }
    private function createConnection(array $config): void
    {
        $dsn = "mysql:dbname={$config['database']};host=${config['host']}";
        $this->connection = new PDO(
            $dsn,
            $config['user'],
            $config['password'],
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        );
    }
    private function validateConfg(array $config): void
    {
        if (
            empty($config['database'])
            || empty($config['host'])
            || empty($config['user'])
            || empty($config['password'])
        ) {
            exit("Złe dane konfig");
        };
    }
}
