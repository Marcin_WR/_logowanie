<?php

declare(strict_types=1);

namespace App;

use App\Exception\ConfigurationException;

class Controller
{
    private const DEFAULT_ACTION = 'login';

    private static $configuration = [];

    private Database $database;
    private View $view;
    private array $request;

    private string $action;
    private string $page;


    public static function initConfiguration(array $configuration): void
    {
        self::$configuration = $configuration;
    }

    public function __construct(array $request)
    {
        if (empty(self::$configuration['db'])) {
            throw new ConfigurationException('Configuration error');
        }
        $this->database = new Database(self::$configuration['db']);
        $this->request = $request;

        $this->view = new View();
    }

    public function run(): void
    {

        if (!empty($this->getRequestPost())) {
            $xd = $this->database->loginPassword($_POST['login'], $_POST['haslo']);
        }

        switch ($this->action()) {
            case 'login':
                $this->page = 'login';
                break;
            case 'logout':
                $_SESSION['zalogowany'] = 1;
                session_unset();
                $this->page = 'login';
                break;
            default:
                if (!empty($xd)) {
                    header("Location: /");
                    $_SESSION['zalogowany'] = 0;
                    $_SESSION['id'] = $xd['id'];
                    $_SESSION['user'] = $xd['user'];
                    $_SESSION['email'] = $xd['email'];
                    unset($_SESSION['blad']);

                    $this->page = 'page1';
                } else {
                    $_SESSION['blad'] = '<span style = color:red>Niepoprawny login lub haslo!</span>';
                    $this->page = 'login';
                }
                break;
        }

        if (isset($_SESSION['zalogowany'])) {
            if ($_SESSION['zalogowany'] == 0) {
                $this->page = 'page1';
            } else {
                $this->page = 'login';
            }
        }
        $this->view->render($this->page);
    }
    private function action(): string
    {
        $data = $this->getRequestGet();
        return $data['action'] ?? self::DEFAULT_ACTION;
    }

    private function getRequestGet(): array
    {
        return $this->request['get'] ?? [];
    }
    private function getRequestPost(): array
    {
        return $this->request['post'] ?? [];
    }
}
