<?php

declare(strict_types=1);

namespace App;

session_start();

require_once("src/Utils/debug.php");
require_once("src/View.php");
require_once("src/Database.php");
require_once("src/Controller.php");




$conn = require_once("config/config.php"); 
$databasee = new Database($conn['db']); 
$request = [
    'get' => $_GET,
    'post' => $_POST
  ];

Controller::initConfiguration($conn);
$controller = new Controller($request);
$controller->run();
